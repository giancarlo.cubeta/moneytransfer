You need to work with legacy money transfer system described as below.
Money can be transferred from source account to destination. 
After successful transaction accounts should be updated with proper amounts of money. 
Account balance cannot be negative number. Please also remember to collect transfer fees depending on transfer type.
There is business requirement to support 3 transfer types:
 
	- domestic: standard one
 	- priority: should be processed first, additional fee is charged, fixed amount
	- international: additional fee is charged, percentage value 

Transfers are related with accounts. There are two account types:
	
	- Standard
	- Loyalty: it holds information about loyalty card and points, this feature will be developed in the future.

--------------------------------------------------------------------------------------------------------------------

ENDPOINTS

------------------------------------------------------

TO GET LIST OF ACCOUNTS
localhost:8080/api/accounts

------------------------------------------------------

TO GET SINGLE ACCOUNT
localhost:8080/api/accounts/3

------------------------------------------------------

TO TRANSFER MONEY BETWEEN ACCOUNTS
localhost:8080/api/moneytransfer

WITH HEADER
content-type ?application/json

WITH BODY
{
	"idSourceAccount": 3,
	"idDestinationAccount": 5,
	"type": "Standard",
	"value": 500
}

------------------------------------------------------
