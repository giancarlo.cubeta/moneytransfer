package com.cubeta.giancarlo.moneytransfer;

import com.cubeta.giancarlo.moneytransfer.loaders.Loader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("!test")
public class CmdLineRunner implements org.springframework.boot.CommandLineRunner {

    private final Loader loaderAccountsFromFile;

    @Autowired
    public CmdLineRunner(Loader loaderAccountsFromFile) {
        this.loaderAccountsFromFile = loaderAccountsFromFile;
    }

    @Override
    public void run(String... args) {
        loaderAccountsFromFile.load();
    }
}
