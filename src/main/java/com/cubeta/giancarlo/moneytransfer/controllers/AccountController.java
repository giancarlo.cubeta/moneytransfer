package com.cubeta.giancarlo.moneytransfer.controllers;

import com.cubeta.giancarlo.moneytransfer.entities.Account;
import com.cubeta.giancarlo.moneytransfer.loaders.LoaderAccountsFromFile;
import com.cubeta.giancarlo.moneytransfer.models.MoneyTransferDTO;
import com.cubeta.giancarlo.moneytransfer.models.MoneyTransferRequest;
import com.cubeta.giancarlo.moneytransfer.models.MoneyTransferResponse;
import com.cubeta.giancarlo.moneytransfer.services.AccountService;
import com.cubeta.giancarlo.moneytransfer.utilities.Parameters;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = Parameters.URI_BASE)
public class AccountController {

    private static final Logger LOG = LoggerFactory.getLogger(LoaderAccountsFromFile.class);
    private final AccountService accountService;
    private final CustomResponseBody customResponseBody;

    @Autowired
    public AccountController(AccountService accountService, CustomResponseBody customResponseBody) {
        this.accountService = accountService;
        this.customResponseBody = customResponseBody;
    }

    @PostMapping(value = Parameters.URI_TRANSFER)
    public ResponseEntity<CustomResponseBody> moneyTransfer(@Valid @RequestBody MoneyTransferRequest moneyTransferRequest, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            LOG.error(bindingResult.toString());
            return new ResponseEntity<>(customResponseBody.with(HttpStatus.BAD_REQUEST, bindingResult.toString()), HttpStatus.BAD_REQUEST);
        }
        ModelMapper modelMapper = new ModelMapper();
        MoneyTransferDTO moneyTransferDTO = modelMapper.map(moneyTransferRequest, MoneyTransferDTO.class);
        MoneyTransferResponse moneyTransferResponse = modelMapper.map(accountService.moneyTransfer(moneyTransferDTO), MoneyTransferResponse.class);
        return new ResponseEntity<>(customResponseBody.with(HttpStatus.OK, moneyTransferResponse), HttpStatus.OK);

    }

    @GetMapping(value = Parameters.URI_ACCOUNTS)
    public ResponseEntity<CustomResponseBody> getAccounts() {
        List<Account> accountList = accountService.findAll();
        return new ResponseEntity<>(customResponseBody.with(HttpStatus.OK, accountList), HttpStatus.OK);
    }

    @GetMapping(value = Parameters.URI_ACCOUNTS + "/{id}")
    public ResponseEntity<CustomResponseBody> findById(@PathVariable @NotNull @DecimalMin("0") Long id) {
        Optional<Account> optAccount = accountService.findById(id);
        return new ResponseEntity<>(customResponseBody.with(HttpStatus.OK, optAccount.orElse(null)), HttpStatus.OK);
    }

}