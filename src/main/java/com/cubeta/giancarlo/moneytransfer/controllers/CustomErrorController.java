package com.cubeta.giancarlo.moneytransfer.controllers;

import com.cubeta.giancarlo.moneytransfer.loaders.LoaderAccountsFromFile;
import com.cubeta.giancarlo.moneytransfer.utilities.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
public class CustomErrorController implements ErrorController {

    private static final Logger LOG = LoggerFactory.getLogger(LoaderAccountsFromFile.class);
    private static final String ERROR_PATH = Parameters.ERROR_PATH;
    private final @NotNull CustomResponseBody customResponseBody;

    @Autowired
    public CustomErrorController(@NotNull CustomResponseBody customResponseBody) {
        this.customResponseBody = customResponseBody;
    }

    @RequestMapping(value = ERROR_PATH)
    public ResponseEntity<CustomResponseBody> error() {
        LOG.error(Parameters.RESOURCE_NOT_FOUND);
        return new ResponseEntity<>(customResponseBody.with(HttpStatus.NOT_FOUND, Parameters.RESOURCE_NOT_FOUND), HttpStatus.NOT_FOUND);
    }

    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }
}