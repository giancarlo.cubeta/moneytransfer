package com.cubeta.giancarlo.moneytransfer.controllers;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Data
@Component
public class CustomResponseBody {

    @NotNull
    private HttpStatus httpStatus;
    @NotNull
    private Object response;

    CustomResponseBody with(@NotNull HttpStatus httpStatus, @NotNull Object response) {
        this.httpStatus = httpStatus;
        this.response = response;
        return this;
    }
}
