package com.cubeta.giancarlo.moneytransfer.controllers;

import com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAccountException;
import com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAmountException;
import com.cubeta.giancarlo.moneytransfer.loaders.LoaderAccountsFromFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

@ControllerAdvice
@RequestMapping(value = "${uri.base}")
public class ExceptionHandlerAdvise {

    private static final Logger LOG = LoggerFactory.getLogger(LoaderAccountsFromFile.class);
    private final CustomResponseBody customResponseBody;

    @Autowired
    public ExceptionHandlerAdvise(CustomResponseBody customResponseBody) {
        this.customResponseBody = customResponseBody;
    }

    @ExceptionHandler(InvalidAccountException.class)
    public ResponseEntity<CustomResponseBody> invalidAccountException(final InvalidAccountException e) {
        LOG.error(e.toString());
        return new ResponseEntity<>(customResponseBody.with(HttpStatus.BAD_REQUEST, e.toString()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidAmountException.class)
    public ResponseEntity<CustomResponseBody> invalidAmountException(final InvalidAmountException e) {
        LOG.error(e.toString());
        return new ResponseEntity<>(customResponseBody.with(HttpStatus.BAD_REQUEST, e.toString()), HttpStatus.BAD_REQUEST);
    }

}