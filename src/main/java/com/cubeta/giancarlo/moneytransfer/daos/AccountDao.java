package com.cubeta.giancarlo.moneytransfer.daos;

import com.cubeta.giancarlo.moneytransfer.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountDao extends JpaRepository<Account, Long> {

}
