package com.cubeta.giancarlo.moneytransfer.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    @NotNull
    private Long id;

    @Column(name = "type", nullable = false)
    @NotNull
    private String type;

    @Column(name = "amount", nullable = false, precision = 11, scale = 2)
    @NotNull
    private BigDecimal amount;

    @Column(name = "currency", nullable = false)
    @NotNull
    private String currency;

    @Column(name = "active", nullable = false)
    @NotNull
    private Boolean active;

    @Column(name = "last_update", nullable = false)
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastUpdate;

    @PrePersist
    private void getTimeOperation() {
        this.lastUpdate = LocalDateTime.now();
    }
}
