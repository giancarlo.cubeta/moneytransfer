package com.cubeta.giancarlo.moneytransfer.exceptions;

public class InvalidAccountException extends RuntimeException {
    public InvalidAccountException(String errorMessage) {
        super(errorMessage);
    }
}