package com.cubeta.giancarlo.moneytransfer.exceptions;

public class InvalidAmountException extends RuntimeException {
    public InvalidAmountException(String errorMessage) {
        super(errorMessage);
    }
}