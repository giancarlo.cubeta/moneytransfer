package com.cubeta.giancarlo.moneytransfer.helpers;

import com.cubeta.giancarlo.moneytransfer.daos.AccountDao;
import com.cubeta.giancarlo.moneytransfer.entities.Account;
import com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAccountException;
import com.cubeta.giancarlo.moneytransfer.loaders.LoaderAccountsFromFile;
import com.cubeta.giancarlo.moneytransfer.models.MoneyTransferDTO;
import com.cubeta.giancarlo.moneytransfer.utilities.Parameters;
import com.cubeta.giancarlo.moneytransfer.utilities.TransferFeeCalculator;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.math.BigDecimal;
import java.util.Optional;

@Setter(AccessLevel.PRIVATE)
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DataTransferReader {

    private static final Logger LOG = LoggerFactory.getLogger(LoaderAccountsFromFile.class);
    private static final DataTransferReader DATA_TRANSFER_READER = new DataTransferReader();
    private MoneyTransferDTO moneyTransferDTO;
    private Long idSourceAccount;
    private Long idDestinationAccount;
    private Account sourceAccount;
    private Account destinationAccount;
    private BigDecimal transferFee;

    public static DataTransferReader getIstanceWith(MoneyTransferDTO moneyTransferDTO, AccountDao accountDao) {

        Long idSourceAccount = moneyTransferDTO.getIdSourceAccount();
        Long idDestinationAccount = moneyTransferDTO.getIdDestinationAccount();
        Optional<Account> optSourceAccount = accountDao.findById(idSourceAccount);
        Optional<Account> optDestinationAccount = accountDao.findById(idDestinationAccount);
        validate(moneyTransferDTO, optSourceAccount, optDestinationAccount);

        DATA_TRANSFER_READER.setMoneyTransferDTO(moneyTransferDTO);
        DATA_TRANSFER_READER.setIdSourceAccount(idSourceAccount);
        DATA_TRANSFER_READER.setIdDestinationAccount(idDestinationAccount);
        DATA_TRANSFER_READER.setSourceAccount(optSourceAccount.get());
        DATA_TRANSFER_READER.setDestinationAccount(optDestinationAccount.get());
        DATA_TRANSFER_READER.setTransferFee(TransferFeeCalculator.getTransferFee(moneyTransferDTO));
        return DATA_TRANSFER_READER;
    }

    private static void validate(MoneyTransferDTO moneyTransferDTO, Optional<Account> optSourceAccount, Optional<Account> optDestinationAccount) {

        Long idSourceAccount = moneyTransferDTO.getIdSourceAccount();
        Long idDestinationAccount = moneyTransferDTO.getIdDestinationAccount();
        boolean sourceAccountIsPresent = optSourceAccount.isPresent();
        boolean destinationAccountIsPresent = optDestinationAccount.isPresent();

        if (!sourceAccountIsPresent) {
            moneyTransferDTO.setStatus(Parameters.INVALID_ACCOUNT_STATUS);
            String msgError = Parameters.NO_ANY_ACCOUNT_FOR_ID + idSourceAccount;
            LOG.error(msgError);
            throw new InvalidAccountException(msgError);
        }

        if (!destinationAccountIsPresent) {
            moneyTransferDTO.setStatus(Parameters.INVALID_ACCOUNT_STATUS);
            String msgError = Parameters.NO_ANY_ACCOUNT_FOR_ID + idDestinationAccount;
            LOG.error(msgError);
            throw new InvalidAccountException(msgError);
        }
    }


}
