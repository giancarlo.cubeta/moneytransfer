package com.cubeta.giancarlo.moneytransfer.loaders;

public interface Loader {
    boolean load();
}
