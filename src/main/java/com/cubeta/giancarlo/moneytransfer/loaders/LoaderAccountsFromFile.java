package com.cubeta.giancarlo.moneytransfer.loaders;

import com.cubeta.giancarlo.moneytransfer.daos.AccountDao;
import com.cubeta.giancarlo.moneytransfer.entities.Account;
import com.cubeta.giancarlo.moneytransfer.utilities.Parameters;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Component
public final class LoaderAccountsFromFile implements Loader {

    private static final Logger LOG = LoggerFactory.getLogger(LoaderAccountsFromFile.class);
    private final AccountDao accountDao;

    @Value(Parameters.ACCOUNTS_FILE_NAMEPATH)
    String accountFileNamePath;

    @Autowired
    public LoaderAccountsFromFile(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public boolean load() {
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<List<Account>> typeReference = new TypeReference<List<Account>>() {
        };
        try (InputStream inputStream = TypeReference.class.getResourceAsStream(accountFileNamePath)) {
            List<Account> accountList = mapper.readValue(inputStream, typeReference);
            accountDao.saveAll(accountList);
            LOG.info(Parameters.ACCOUNTS_LOADED_MSG);
            return true;
        } catch (IOException e) {
            LOG.error(Parameters.UNABLE_TO_LOAD_MSG + e.getMessage());
            return false;
        }
    }


}
