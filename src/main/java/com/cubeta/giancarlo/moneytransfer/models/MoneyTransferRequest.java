package com.cubeta.giancarlo.moneytransfer.models;

import lombok.Value;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Value
public class MoneyTransferRequest {

    @NotNull
    private Long idSourceAccount;

    @NotNull
    private Long idDestinationAccount;

    @NotNull
    private BigDecimal value;

    @NotNull
    private String type;

}
