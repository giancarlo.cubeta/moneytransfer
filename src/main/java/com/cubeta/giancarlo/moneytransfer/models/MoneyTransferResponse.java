package com.cubeta.giancarlo.moneytransfer.models;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class MoneyTransferResponse {

    @NotNull
    private Long idSourceAccount;

    @NotNull
    private Long idDestinationAccount;

    @NotNull
    private BigDecimal value;

    @NotNull
    private String type;

    @NotNull
    private String status;

    @NotNull
    private BigDecimal initialAmountSourceAccount;

    @NotNull
    private BigDecimal finalAmountSourceAccount;

    @NotNull
    private BigDecimal initialAmountDestinationAccount;

    @NotNull
    private BigDecimal finalAmountDestinationAccount;

    @NotNull
    private BigDecimal transferFee;

}
