package com.cubeta.giancarlo.moneytransfer.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum MoneyTransferType {
    STANDARD_ACCOUNT("Standard"),
    PRIORITY_ACCOUNT("Priority"),
    INTERNATIONAL_ACCOUNT("International");

    private final String name;
}
