package com.cubeta.giancarlo.moneytransfer.services;

import com.cubeta.giancarlo.moneytransfer.entities.Account;
import com.cubeta.giancarlo.moneytransfer.models.MoneyTransferDTO;

import java.util.List;
import java.util.Optional;

public interface AccountService {

    List<Account> findAll();

    Optional<Account> findById(Long id);

    MoneyTransferDTO moneyTransfer(MoneyTransferDTO moneyTransferDTO);

}
