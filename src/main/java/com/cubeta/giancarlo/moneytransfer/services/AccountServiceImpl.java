package com.cubeta.giancarlo.moneytransfer.services;

import com.cubeta.giancarlo.moneytransfer.daos.AccountDao;
import com.cubeta.giancarlo.moneytransfer.entities.Account;
import com.cubeta.giancarlo.moneytransfer.helpers.DataTransferReader;
import com.cubeta.giancarlo.moneytransfer.models.MoneyTransferDTO;
import com.cubeta.giancarlo.moneytransfer.utilities.TransferProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AccountServiceImpl implements AccountService {

    private final AccountDao accountDao;
    private final TransferProcessor transferProcessor;
    private final com.cubeta.giancarlo.moneytransfer.services.checker.transferChecker transferChecker;

    @Autowired
    public AccountServiceImpl(AccountDao accountDao, TransferProcessor transferProcessor, com.cubeta.giancarlo.moneytransfer.services.checker.transferChecker transferChecker) {
        this.accountDao = accountDao;
        this.transferProcessor = transferProcessor;
        this.transferChecker = transferChecker;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, isolation = Isolation.REPEATABLE_READ)
    public MoneyTransferDTO moneyTransfer(MoneyTransferDTO moneyTransferDTO) {

        DataTransferReader dtr = DataTransferReader.getIstanceWith(moneyTransferDTO, accountDao);
        transferChecker.check(dtr);
        transferProcessor.processTransfer(dtr);
        return moneyTransferDTO;
    }

    @Override
    public List<Account> findAll() {
        return accountDao.findAll();
    }

    @Override
    public Optional<Account> findById(Long id) {
        return accountDao.findById(id);
    }


}
