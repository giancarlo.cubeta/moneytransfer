package com.cubeta.giancarlo.moneytransfer.services.checker;

import com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAmountException;
import com.cubeta.giancarlo.moneytransfer.loaders.LoaderAccountsFromFile;
import com.cubeta.giancarlo.moneytransfer.helpers.DataTransferReader;
import com.cubeta.giancarlo.moneytransfer.utilities.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@Qualifier("AmountChecker")
public class AmountChecker implements Checker {

    private static final Logger LOG = LoggerFactory.getLogger(LoaderAccountsFromFile.class);

    @Override
    public void check(DataTransferReader dtr) {
        checkAmount(dtr);
    }

    private void checkAmount(DataTransferReader dtr) {

        BigDecimal sourceAccountAmount = dtr.getSourceAccount().getAmount();
        BigDecimal moneyTransferDTOValue = dtr.getMoneyTransferDTO().getValue();
        BigDecimal transferFee = dtr.getTransferFee();

        int condition1 = sourceAccountAmount.compareTo(moneyTransferDTOValue.add(transferFee));
        int condition2 = sourceAccountAmount.compareTo(BigDecimal.ZERO);
        int condition3 = moneyTransferDTOValue.compareTo(BigDecimal.ZERO);

        if (condition1 < 0 || condition2 <= 0 || condition3 <= 0) {
            dtr.getMoneyTransferDTO().setStatus(Parameters.INVALID_AMOUNT_STATUS);
            String msgError = Parameters.AMOUNT_NOT_VALID_FOR_ACCOUNT_ID + dtr.getIdSourceAccount();
            LOG.error(msgError);
            throw new InvalidAmountException(msgError);
        }
    }
}
