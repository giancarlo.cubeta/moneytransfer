package com.cubeta.giancarlo.moneytransfer.services.checker;

import com.cubeta.giancarlo.moneytransfer.helpers.DataTransferReader;

public interface Checker {
    void check(DataTransferReader dtr);
}
