package com.cubeta.giancarlo.moneytransfer.services.checker;

import com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAccountException;
import com.cubeta.giancarlo.moneytransfer.loaders.LoaderAccountsFromFile;
import com.cubeta.giancarlo.moneytransfer.helpers.DataTransferReader;
import com.cubeta.giancarlo.moneytransfer.utilities.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("CurrencyChecker")
public class CurrencyChecker implements Checker {

    private static final Logger LOG = LoggerFactory.getLogger(LoaderAccountsFromFile.class);

    public void check(DataTransferReader dtr) {
        checkCurrency(dtr);
    }

    private void checkCurrency(DataTransferReader dtr) {

        String sourceAccountCurrency = dtr.getSourceAccount().getCurrency();
        String destinationAccountCurrency = dtr.getDestinationAccount().getCurrency();

        if (!sourceAccountCurrency.equals(destinationAccountCurrency)) {
            dtr.getMoneyTransferDTO().setStatus(Parameters.INVALID_AMOUNT_STATUS);
            String msgError = String.format(Parameters.DIFFERENT_CURRENCY_ACCOUNT_MSG, dtr.getIdSourceAccount(), dtr.getIdDestinationAccount());
            LOG.error(msgError);
            throw new InvalidAccountException(msgError);
        }
    }
}
