package com.cubeta.giancarlo.moneytransfer.services.checker;

import com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAccountException;
import com.cubeta.giancarlo.moneytransfer.loaders.LoaderAccountsFromFile;
import com.cubeta.giancarlo.moneytransfer.helpers.DataTransferReader;
import com.cubeta.giancarlo.moneytransfer.utilities.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("IfActiveChecker")
public class IfActiveChecker implements Checker {

    private static final Logger LOG = LoggerFactory.getLogger(LoaderAccountsFromFile.class);

    @Override
    public void check(DataTransferReader dtr) {
        checkIfActive(dtr);
    }

    private void checkIfActive(DataTransferReader dtr) {

        boolean sourceAccountActive = dtr.getSourceAccount().getActive();
        boolean destinationAccountActive = dtr.getDestinationAccount().getActive();

        if (!sourceAccountActive) {
            dtr.getMoneyTransferDTO().setStatus(Parameters.INVALID_ACCOUNT_STATUS);
            String msgError = Parameters.NO_ACTIVE_ACCOUNT_FOR_ID + dtr.getIdSourceAccount();
            LOG.error(msgError);
            throw new InvalidAccountException(msgError);
        }

        if (!destinationAccountActive) {
            dtr.getMoneyTransferDTO().setStatus(Parameters.INVALID_ACCOUNT_STATUS);
            String msgError = Parameters.NO_ACTIVE_ACCOUNT_FOR_ID + dtr.getIdDestinationAccount();
            LOG.error(msgError);
            throw new InvalidAccountException(msgError);
        }
    }
}
