package com.cubeta.giancarlo.moneytransfer.services.checker;

import com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAccountException;
import com.cubeta.giancarlo.moneytransfer.loaders.LoaderAccountsFromFile;
import com.cubeta.giancarlo.moneytransfer.helpers.DataTransferReader;
import com.cubeta.giancarlo.moneytransfer.utilities.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("ValidIdChecker")
public class ValidIdChecker implements Checker {

    private static final Logger LOG = LoggerFactory.getLogger(LoaderAccountsFromFile.class);

    @Override
    public void check(DataTransferReader dtr) {
        checkValidId(dtr);
    }

    private void checkValidId(DataTransferReader dtr) {

        if (dtr.getIdSourceAccount().compareTo(dtr.getIdDestinationAccount()) == 0) {
            dtr.getMoneyTransferDTO().setStatus(Parameters.INVALID_ACCOUNT_STATUS);
            String msgError = Parameters.SAME_ACCOUNT_TRANSFER + dtr.getIdSourceAccount();
            LOG.error(msgError);
            throw new InvalidAccountException(msgError);
        }
    }
}
