package com.cubeta.giancarlo.moneytransfer.services.checker;

import com.cubeta.giancarlo.moneytransfer.helpers.DataTransferReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class transferChecker implements Checker {

    private final Checker ValidIdChecker;
    private final Checker IfActiveChecker;
    private final Checker CurrencyChecker;
    private final Checker AmountChecker;

    @Autowired
    public transferChecker(@Qualifier("ValidIdChecker") Checker ValidIdChecker, @Qualifier("IfActiveChecker") Checker IfActiveChecker, @Qualifier("CurrencyChecker") Checker CurrencyChecker, @Qualifier("AmountChecker") Checker AmountChecker) {
        this.ValidIdChecker = ValidIdChecker;
        this.IfActiveChecker = IfActiveChecker;
        this.CurrencyChecker = CurrencyChecker;
        this.AmountChecker = AmountChecker;
    }

    @Override
    public void check(DataTransferReader dtr) {
        ValidIdChecker.check(dtr);
        IfActiveChecker.check(dtr);
        CurrencyChecker.check(dtr);
        AmountChecker.check(dtr);
    }
}
