package com.cubeta.giancarlo.moneytransfer.utilities;

import java.math.BigDecimal;

public class Parameters {

    public static final String URI_BASE = "/api";
    public static final String URI_ACCOUNTS = "/accounts";
    public static final String ACCOUNTS_FILE_NAMEPATH = "/json/accounts.json";
    public static final String OK_STATUS = "Transferred";
    public static final String INVALID_ACCOUNT_STATUS = "BadAccount";
    public static final String INVALID_AMOUNT_STATUS = "BadAmount";
    public static final String URI_TRANSFER = "/moneytransfer";
    public static final BigDecimal STANDARD_ACCOUNT_FIXED_FEE = BigDecimal.valueOf(0);
    public static final BigDecimal STANDARD_ACCOUNT_PERCENTAGE_FEE = BigDecimal.valueOf(0);
    public static final BigDecimal PRIORITY_ACCOUNT_FIXED_FEE = BigDecimal.valueOf(5);
    public static final BigDecimal PRIORITY_ACCOUNT_PERCENTAGE_FEE = BigDecimal.valueOf(0);
    public static final BigDecimal INTERNATIONAL_ACCOUNT_FIXED_FEE = BigDecimal.valueOf(0);
    public static final BigDecimal INTERNATIONAL_ACCOUNT_PERCENTAGE_FEE = BigDecimal.valueOf(0.03);
    public static final int PERCENTAGE_DECIMAL = 2;
    public static final String ACCOUNTS_LOADED_MSG = "accounts loaded";
    public static final String UNABLE_TO_LOAD_MSG = "Unable to load accounts: ";
    public static final String NO_ANY_ACCOUNT_FOR_ID = "No any account for id: ";
    public static final String NO_ACTIVE_ACCOUNT_FOR_ID = "No active account for id: ";
    public static final String DIFFERENT_CURRENCY_ACCOUNT_MSG = "Different currency account for accounts with id: %s and account with id: %s";
    public static final String AMOUNT_NOT_VALID_FOR_ACCOUNT_ID = "Amount not valid for account id: ";
    public static final String RESOURCE_NOT_FOUND = "Resource not found";
    public static final String ERROR_PATH = "/error";
    public static final String SAME_ACCOUNT_TRANSFER = "Source account and destination account have the same id: ";
}
