package com.cubeta.giancarlo.moneytransfer.utilities;

import com.cubeta.giancarlo.moneytransfer.models.MoneyTransferDTO;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.cubeta.giancarlo.moneytransfer.models.MoneyTransferType.*;
import static com.cubeta.giancarlo.moneytransfer.utilities.Parameters.PERCENTAGE_DECIMAL;

public class TransferFeeCalculator {

    public static BigDecimal getTransferFee(MoneyTransferDTO moneyTransferDTO) {
        BigDecimal transferFee;
        if (STANDARD_ACCOUNT.getName().equals(moneyTransferDTO.getType())) {
            transferFee = buildTransferFee(moneyTransferDTO.getValue(), Parameters.STANDARD_ACCOUNT_FIXED_FEE, Parameters.STANDARD_ACCOUNT_PERCENTAGE_FEE);
        } else if (PRIORITY_ACCOUNT.getName().equals(moneyTransferDTO.getType())) {
            transferFee = buildTransferFee(moneyTransferDTO.getValue(), Parameters.PRIORITY_ACCOUNT_FIXED_FEE, Parameters.PRIORITY_ACCOUNT_PERCENTAGE_FEE);
        } else if (INTERNATIONAL_ACCOUNT.getName().equals(moneyTransferDTO.getType())) {
            transferFee = buildTransferFee(moneyTransferDTO.getValue(), Parameters.INTERNATIONAL_ACCOUNT_FIXED_FEE, Parameters.INTERNATIONAL_ACCOUNT_PERCENTAGE_FEE);
        } else {
            transferFee = BigDecimal.ZERO;
        }
        return transferFee;
    }

    private static BigDecimal buildTransferFee(BigDecimal moneyTransferDTOValue, BigDecimal accountFixedFee, BigDecimal accountPercentageFee) {
        return accountFixedFee.add(moneyTransferDTOValue.multiply(accountPercentageFee)).setScale(PERCENTAGE_DECIMAL, RoundingMode.CEILING);
    }
}

