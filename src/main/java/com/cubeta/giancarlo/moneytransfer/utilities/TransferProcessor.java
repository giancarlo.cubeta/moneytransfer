package com.cubeta.giancarlo.moneytransfer.utilities;

import com.cubeta.giancarlo.moneytransfer.daos.AccountDao;
import com.cubeta.giancarlo.moneytransfer.entities.Account;
import com.cubeta.giancarlo.moneytransfer.helpers.DataTransferReader;
import com.cubeta.giancarlo.moneytransfer.models.MoneyTransferDTO;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class TransferProcessor {

    private final AccountDao accountDao;

    public TransferProcessor(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public void processTransfer(DataTransferReader dtr) {

        Account sourceAccount = dtr.getSourceAccount();
        Account destinationAccount = dtr.getDestinationAccount();
        BigDecimal transferFee = dtr.getTransferFee();
        MoneyTransferDTO moneyTransferDTO = dtr.getMoneyTransferDTO();

        BigDecimal sourceAccountAmount = sourceAccount.getAmount();
        BigDecimal destinationAccountAmount = destinationAccount.getAmount();
        BigDecimal moneyTransferDTOValueWithFee = moneyTransferDTO.getValue().add(transferFee);
        BigDecimal sourceAccountAmountReduced = sourceAccountAmount.subtract(moneyTransferDTOValueWithFee);
        BigDecimal destinationAccountAmountIncreased = destinationAccountAmount.add(moneyTransferDTO.getValue());
        sourceAccount.setAmount(sourceAccountAmountReduced);
        destinationAccount.setAmount(destinationAccountAmountIncreased);

        accountDao.save(sourceAccount);
        accountDao.save(destinationAccount);

        //Check Roll back
        //if (true ) throw new RuntimeException("Roll back");

        moneyTransferDTO.setTransferFee(transferFee);
        moneyTransferDTO.setInitialAmountSourceAccount(sourceAccountAmount);
        moneyTransferDTO.setInitialAmountDestinationAccount(destinationAccountAmount);
        moneyTransferDTO.setFinalAmountSourceAccount(sourceAccountAmountReduced);
        moneyTransferDTO.setFinalAmountDestinationAccount(destinationAccountAmountIncreased);
        moneyTransferDTO.setStatus(Parameters.OK_STATUS);

    }
}
