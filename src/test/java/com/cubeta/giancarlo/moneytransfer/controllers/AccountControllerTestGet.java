package com.cubeta.giancarlo.moneytransfer.controllers;

import com.cubeta.giancarlo.moneytransfer.daos.AccountDao;
import com.cubeta.giancarlo.moneytransfer.models.MoneyTransferDTO;
import com.cubeta.giancarlo.moneytransfer.utilities.DbLoader;
import com.cubeta.giancarlo.moneytransfer.utilities.Parameters;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import org.junit.Test;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test") //Necessary to block the FileLoader
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class AccountControllerTestGet {

    @Autowired
    AccountDao accountDao;
    @LocalServerPort
    private int port;
    @Autowired
    private MoneyTransferDTO moneyTransferDTO;

    private String url;

    @Before
    public void setup() {
        DbLoader.load(accountDao);
        url = "http://localhost:" + port + Parameters.URI_BASE + Parameters.URI_ACCOUNTS;
    }

    @Test
    public void testGetAccounts() {
        given().
                contentType("application/json").
                when().
                get(url).
                then().
                statusCode(200).
                body(containsString("OK"));
    }

    @Test
    public void testGetAccountByID() {
        given().
                contentType("application/json").
                when().
                get(url + "/3").
                then().
                statusCode(200).
                body(containsString("currency"));
    }

}