package com.cubeta.giancarlo.moneytransfer.controllers;

import com.cubeta.giancarlo.moneytransfer.daos.AccountDao;
import com.cubeta.giancarlo.moneytransfer.models.MoneyTransferDTO;
import com.cubeta.giancarlo.moneytransfer.utilities.DbLoader;
import com.cubeta.giancarlo.moneytransfer.utilities.Parameters;
import com.cubeta.giancarlo.moneytransfer.utilities.StringUtils;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import java.math.BigDecimal;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test") //Necessary to block the FileLoader
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class AccountControllerTestPost {

    private final TestRestTemplate restTemplate = new TestRestTemplate();
    private final HttpHeaders headers = new HttpHeaders();
    @Autowired
    AccountDao accountDao;
    @Autowired
    private MoneyTransferDTO moneyTransferDTO;
    @LocalServerPort
    private int port;
    private String url;

    @Before
    public void setup() {
        DbLoader.load(accountDao);
        url = "http://localhost:" + port + Parameters.URI_BASE + Parameters.URI_TRANSFER;
    }

    @Test
    public void testStandardMoneyTransfer() {

        moneyTransferDTO.setIdSourceAccount(3L);
        moneyTransferDTO.setIdDestinationAccount(5L);
        moneyTransferDTO.setType("Standard");
        moneyTransferDTO.setValue(BigDecimal.valueOf(500));

        HttpEntity<MoneyTransferDTO> entity = new HttpEntity<>(moneyTransferDTO, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        String expected = "{\"httpStatus\":\"OK\",\"response\":{\"idSourceAccount\":3,\"idDestinationAccount\":5,\"value\":500,\"type\":\"Standard\",\"status\":\"Transferred\",\"initialAmountSourceAccount\":3000.00,\"finalAmountSourceAccount\":2500.00,\"initialAmountDestinationAccount\":5000.00,\"finalAmountDestinationAccount\":5500.00,\"transferFee\":0.00}}";
        assertThat(responseEntity.getBody(), is(expected));
    }

    @Test
    public void testPriorityMoneyTransfer() {

        moneyTransferDTO.setIdSourceAccount(3L);
        moneyTransferDTO.setIdDestinationAccount(5L);
        moneyTransferDTO.setType("Priority");
        moneyTransferDTO.setValue(BigDecimal.valueOf(500));

        HttpEntity<MoneyTransferDTO> entity = new HttpEntity<>(moneyTransferDTO, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        String expected = "{\"httpStatus\":\"OK\",\"response\":{\"idSourceAccount\":3,\"idDestinationAccount\":5,\"value\":500,\"type\":\"Priority\",\"status\":\"Transferred\",\"initialAmountSourceAccount\":3000.00,\"finalAmountSourceAccount\":2495.00,\"initialAmountDestinationAccount\":5000.00,\"finalAmountDestinationAccount\":5500.00,\"transferFee\":5.00}}";
        assertThat(responseEntity.getBody(), is(expected));
    }

    @Test
    public void testInternationalMoneyTransfer() {

        moneyTransferDTO.setIdSourceAccount(3L);
        moneyTransferDTO.setIdDestinationAccount(5L);
        moneyTransferDTO.setType("International");
        moneyTransferDTO.setValue(BigDecimal.valueOf(500));

        HttpEntity<MoneyTransferDTO> entity = new HttpEntity<>(moneyTransferDTO, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        String expected = "{\"httpStatus\":\"OK\",\"response\":{\"idSourceAccount\":3,\"idDestinationAccount\":5,\"value\":500,\"type\":\"International\",\"status\":\"Transferred\",\"initialAmountSourceAccount\":3000.00,\"finalAmountSourceAccount\":2485.00,\"initialAmountDestinationAccount\":5000.00,\"finalAmountDestinationAccount\":5500.00,\"transferFee\":15.00}}";
        assertThat(responseEntity.getBody(), is(expected));
    }

    @Test
    public void testStandardMoneyTransferExcessiveValue() {

        moneyTransferDTO.setIdSourceAccount(3L);
        moneyTransferDTO.setIdDestinationAccount(5L);
        moneyTransferDTO.setType("International");
        moneyTransferDTO.setValue(BigDecimal.valueOf(99999999));

        HttpEntity<MoneyTransferDTO> entity = new HttpEntity<>(moneyTransferDTO, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        String expected = "{\"httpStatus\":\"BAD_REQUEST\",\"response\":\"com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAmountException: Amount not valid for account id: 3\"}";
        assertThat(responseEntity.getBody(), is(expected));
    }

    @Test
    public void testStandardMoneyTransferSameId() {

        moneyTransferDTO.setIdSourceAccount(3L);
        moneyTransferDTO.setIdDestinationAccount(3L);
        moneyTransferDTO.setType("International");
        moneyTransferDTO.setValue(BigDecimal.valueOf(10));

        HttpEntity<MoneyTransferDTO> entity = new HttpEntity<>(moneyTransferDTO, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        String expected = "{\"httpStatus\":\"BAD_REQUEST\",\"response\":\"com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAccountException: Source account and destination account have the same id: 3\"}";
        assertThat(responseEntity.getBody(), is(expected));
    }

    @Test
    public void testStandardMoneyTransferInexistentId() {

        moneyTransferDTO.setIdSourceAccount(9999999L);
        moneyTransferDTO.setIdDestinationAccount(3L);
        moneyTransferDTO.setType("International");
        moneyTransferDTO.setValue(BigDecimal.valueOf(10));

        HttpEntity<MoneyTransferDTO> entity = new HttpEntity<>(moneyTransferDTO, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        String expected = "{\"httpStatus\":\"BAD_REQUEST\",\"response\":\"com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAccountException: No any account for id: 9999999\"}";
        assertThat(responseEntity.getBody(), is(expected));
    }

    @Test
    public void testStandardMoneyTransferZeroValue() {

        moneyTransferDTO.setIdSourceAccount(3L);
        moneyTransferDTO.setIdDestinationAccount(5L);
        moneyTransferDTO.setType("International");
        moneyTransferDTO.setValue(BigDecimal.valueOf(0));

        HttpEntity<MoneyTransferDTO> entity = new HttpEntity<>(moneyTransferDTO, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        String expected = "{\"httpStatus\":\"BAD_REQUEST\",\"response\":\"com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAmountException: Amount not valid for account id: 3\"}";
        assertThat(responseEntity.getBody(), is(expected));
    }

    @Test
    public void testStandardMoneyTransferNegativeValue() {

        moneyTransferDTO.setIdSourceAccount(3L);
        moneyTransferDTO.setIdDestinationAccount(5L);
        moneyTransferDTO.setType("International");
        moneyTransferDTO.setValue(BigDecimal.valueOf(-10));

        HttpEntity<MoneyTransferDTO> entity = new HttpEntity<>(moneyTransferDTO, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        String expected = "{\"httpStatus\":\"BAD_REQUEST\",\"response\":\"com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAmountException: Amount not valid for account id: 3\"}";
        assertThat(responseEntity.getBody(), is(expected));
    }

    @Test
    public void testStandardMoneyTransferNegativeAmountDestination() {

        moneyTransferDTO.setIdSourceAccount(5L);
        moneyTransferDTO.setIdDestinationAccount(10L);
        moneyTransferDTO.setType("Standard");
        moneyTransferDTO.setValue(BigDecimal.valueOf(1000));

        HttpEntity<MoneyTransferDTO> entity = new HttpEntity<>(moneyTransferDTO, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        String expected = "{\"httpStatus\":\"OK\",\"response\":{\"idSourceAccount\":5,\"idDestinationAccount\":10,\"value\":1000,\"type\":\"Standard\",\"status\":\"Transferred\",\"initialAmountSourceAccount\":5000.00,\"finalAmountSourceAccount\":4000.00,\"initialAmountDestinationAccount\":-10000.00,\"finalAmountDestinationAccount\":-9000.00,\"transferFee\":0.00}}";
        assertThat(responseEntity.getBody(), is(expected));
    }

    @Test
    public void testStandardMoneyTransferNegativeAmountSource() {

        moneyTransferDTO.setIdSourceAccount(10L);
        moneyTransferDTO.setIdDestinationAccount(5L);
        moneyTransferDTO.setType("Standard");
        moneyTransferDTO.setValue(BigDecimal.valueOf(1000));

        HttpEntity<MoneyTransferDTO> entity = new HttpEntity<>(moneyTransferDTO, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        String expected = "{\"httpStatus\":\"BAD_REQUEST\",\"response\":\"com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAmountException: Amount not valid for account id: 10\"}";
        assertThat(responseEntity.getBody(), is(expected));
    }

    @Test
    public void testStandardMoneyTransferDifferentCurrency() {

        moneyTransferDTO.setIdSourceAccount(8L);
        moneyTransferDTO.setIdDestinationAccount(9L);
        moneyTransferDTO.setType("Standard");
        moneyTransferDTO.setValue(BigDecimal.valueOf(1000));

        HttpEntity<MoneyTransferDTO> entity = new HttpEntity<>(moneyTransferDTO, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        String expected = "{\"httpStatus\":\"BAD_REQUEST\",\"response\":\"com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAccountException: Different currency account for accounts with id: 8 and account with id: 9\"}";
        assertThat(responseEntity.getBody(), is(expected));
    }

    @Test
    public void testStandardMoneyTransferNotActiveSource() {

        moneyTransferDTO.setIdSourceAccount(7L);
        moneyTransferDTO.setIdDestinationAccount(6L);
        moneyTransferDTO.setType("Standard");
        moneyTransferDTO.setValue(BigDecimal.valueOf(1000));

        HttpEntity<MoneyTransferDTO> entity = new HttpEntity<>(moneyTransferDTO, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        String expected = "{\"httpStatus\":\"BAD_REQUEST\",\"response\":\"com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAccountException: No active account for id: 7\"}";
        assertThat(responseEntity.getBody(), is(expected));
    }

    @Test
    public void testStandardMoneyTransferNotActiveDestination() {

        moneyTransferDTO.setIdSourceAccount(6L);
        moneyTransferDTO.setIdDestinationAccount(7L);
        moneyTransferDTO.setType("Standard");
        moneyTransferDTO.setValue(BigDecimal.valueOf(1000));

        HttpEntity<MoneyTransferDTO> entity = new HttpEntity<>(moneyTransferDTO, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        String expected = "{\"httpStatus\":\"BAD_REQUEST\",\"response\":\"com.cubeta.giancarlo.moneytransfer.exceptions.InvalidAccountException: No active account for id: 7\"}";
        assertThat(responseEntity.getBody(), is(expected));
    }

    @Test
    public void testStandardMoneyTransferValidFormat() {
        String jsonBody = StringUtils.generateStringFromResource("./src/test/resources/json/moneyTransferValidFormat.json");
        given().
                contentType("application/json").
                body(jsonBody).
                when().
                post(url).
                then().
                statusCode(200).
                body(containsString("OK"));
    }

    @Test
    public void testStandardMoneyTransferInvalidFormatType() {
        String jsonBody = StringUtils.generateStringFromResource("./src/test/resources/json/moneyTransferInvalidFormatType.json");
        given().
                contentType("application/json").
                body(jsonBody).
                when().
                post(url).
                then().
                statusCode(400).
                body(containsString("BAD_REQUEST"));
    }

    @Test
    public void testStandardMoneyTransferInvalidFormatId() {
        String jsonBody = StringUtils.generateStringFromResource("./src/test/resources/json/moneyTransferInvalidFormatId.json");
        given().
                contentType("application/json").
                body(jsonBody).
                when().
                post(url).
                then().
                statusCode(404).
                body(containsString("NOT_FOUND"));
    }

    @Test
    public void testStandardMoneyTransferInvalidFormatValue() {
        String jsonBody = StringUtils.generateStringFromResource("./src/test/resources/json/moneyTransferInvalidFormatValue.json");
        given().
                contentType("application/json").
                body(jsonBody).
                when().
                post(url).
                then().
                statusCode(404).
                body(containsString("NOT_FOUND"));
    }

    @Test
    public void testStandardMoneyTransferNoTypeField() {
        String jsonBody = StringUtils.generateStringFromResource("./src/test/resources/json/moneyTransferNoTypeField.json");
        given().
                contentType("application/json").
                body(jsonBody).
                when().
                post(url).
                then().
                statusCode(400).
                body(containsString("BAD_REQUEST"));
    }

    @Test
    public void testStandardMoneyTransferSameld() {
        String jsonBody = StringUtils.generateStringFromResource("./src/test/resources/json/moneyTransferSameId.json");
        given().
                contentType("application/json").
                body(jsonBody).
                when().
                post(url).
                then().
                statusCode(400).
                body(containsString("BAD_REQUEST"));
    }

}