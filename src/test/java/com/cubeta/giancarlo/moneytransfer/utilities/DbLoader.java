package com.cubeta.giancarlo.moneytransfer.utilities;

import com.cubeta.giancarlo.moneytransfer.daos.AccountDao;
import com.cubeta.giancarlo.moneytransfer.entities.Account;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class DbLoader {

    public static boolean load(AccountDao accountDao) {
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<List<Account>> typeReference = new TypeReference<List<Account>>() {
        };
        try (InputStream inputStream = TypeReference.class.getResourceAsStream("/json/accounts.json")) {
            List<Account> accountList = mapper.readValue(inputStream, typeReference);
            accountDao.saveAll(accountList);
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}
